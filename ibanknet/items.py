# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class IbanknetItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    state = scrapy.Field()
    name = scrapy.Field()
    year_open = scrapy.Field()
    entity_type = scrapy.Field()
    member_federal_reserve = scrapy.Field()
    fiduciary_powers = scrapy.Field()
    specialization = scrapy.Field()
    statistical_area = scrapy.Field()
    interstate_branches = scrapy.Field()
    number_of_offices = scrapy.Field()
    institution_type = scrapy.Field()
    federal_regulator = scrapy.Field()
    s_corp = scrapy.Field()
    powers_exercised = scrapy.Field()
    foreign_offices = scrapy.Field()
    number_of_employees = scrapy.Field()
    rc_balance_sheet_report = scrapy.Field()
    ri_income_statement_report = scrapy.Field()
    total_assets = scrapy.Field()
    total_liabilities = scrapy.Field()
    total_bank_equity_capital = scrapy.Field()
    domestic_deposits = scrapy.Field()
    total_deposits = scrapy.Field()
    net_loans_leases = scrapy.Field()
    loan_loss_allowance = scrapy.Field()
    total_interest_income = scrapy.Field()
    total_noninterest_income = scrapy.Field()
    total_interest_expense = scrapy.Field()
    total_noninterest_expense = scrapy.Field()
    net_income = scrapy.Field()
    net_charge_offs = scrapy.Field()
    total_capital = scrapy.Field()
    total_assets_leverage_ratio = scrapy.Field()
    tier_leverage_ratio = scrapy.Field()
    tier_capital_ratio = scrapy.Field()
    total_capital_ratio = scrapy.Field()
    total_loans = scrapy.Field()
    loans_secured_real_estate = scrapy.Field()
    commercial_industrial_loans = scrapy.Field()
    loans_individuals_credit_cards = scrapy.Field()
    loans_individuals_other = scrapy.Field()
    url = scrapy.Field()
