# -*- coding: utf-8 -*-

from scrapy.conf import settings
from scrapy.exporters import CsvItemExporter


class CSVkwItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        fields_to_export = ['state', 'name', 'year_open', 'entity_type', 'member_federal_reserve', 'fiduciary_powers',
                            'specialization', 'statistical_area', 'interstate_branches', 'number_of_offices',
                            'institution_type', 'federal_regulator', 's_corp', 'powers_exercised', 'foreign_offices',
                            'number_of_employees', 'rc_balance_sheet_report', 'ri_income_statement_report',
                            'total_assets', 'total_liabilities', 'total_bank_equity_capital', 'domestic_deposits',
                            'total_deposits', 'net_loans_leases', 'loan_loss_allowance', 'total_interest_income',
                            'total_noninterest_income', 'total_interest_expense', 'total_noninterest_expense',
                            'net_income', 'net_charge_offs', 'total_capital', 'total_assets_leverage_ratio',
                            'tier_leverage_ratio', 'tier_capital_ratio', 'total_capital_ratio', 'total_loans',
                            'loans_secured_real_estate', 'commercial_industrial_loans',
                            'loans_individuals_credit_cards', 'loans_individuals_other', 'url']
        kwargs['fields_to_export'] = fields_to_export or None
        kwargs['encoding'] = settings.get('EXPORT_ENCODING', 'utf-8')

        super(CSVkwItemExporter, self).__init__(*args, **kwargs)

