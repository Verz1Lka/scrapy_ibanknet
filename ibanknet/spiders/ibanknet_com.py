# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from ibanknet.items import IbanknetItem


class IbanknetComSpider(scrapy.Spider):
    name = "ibanknet.com"
    allowed_domains = ["ibanknet.com"]
    start_urls = (
        'http://ibanknet.com/bankmap/index.shtml',
    )

    def parse(self, response):
        for link in response.xpath('//map[@name="ibnallstates"]/area/@href').extract():
            state = link.split('=')[-1]
            rq = scrapy.Request(response.urljoin(link), callback = self.get_list)
            rq.meta['state'] = state
            yield rq
        return

    def get_list(self, response):
        link = ' '.join(response.xpath('//a[@class="pagebody"][text()="Bank"]/@href').extract())
        rq = scrapy.Request(response.urljoin(link),  callback = self.get_banks)
        rq.meta['state'] = response.meta['state']
        yield rq
        return

    def get_banks(self, response):
        for link in response.xpath('//td[@class="pagebody"]/a/@href').extract():
            rq = scrapy.Request(response.urljoin(link), callback=self.get_data)
            rq.meta['state'] = response.meta['state']
            yield rq
        return

    def get_data(self, response):
        l = IbanknetComLoader(item=IbanknetItem(), response=response)
        l.add_value('state', response.meta['state'])
        l.add_xpath('name', '//div[@class="blockuhilite"]/font/text()')
        l.add_xpath('year_open', '//center[2]/table/tr[2]/td[2]/b/text()')
        l.add_xpath('entity_type', '//center[2]/table/tr[3]/td[2]/b/text()')
        l.add_xpath('member_federal_reserve', '//center[2]/table/tr[4]/td[2]/b/text()')
        l.add_xpath('fiduciary_powers', '//center[2]/table/tr[5]/td[2]/b/text()')
        l.add_xpath('specialization', '//center[2]/table/tr[7]/td[2]/b/text()')
        l.add_xpath('statistical_area', '//center[2]/table/tr[8]/td[2]/b/text()')
        l.add_xpath('interstate_branches', '//center[2]/table/tr[9]/td[2]/b/text()')
        l.add_xpath('number_of_offices', '//center[2]/table/tr[10]/td[2]/b/text()')
        l.add_xpath('institution_type', '//center[2]/table/tr[2]/td[4]/b/text()')
        l.add_xpath('federal_regulator', '//center[2]/table/tr[3]/td[4]/b/text()')
        l.add_xpath('s_corp', '//center[2]/table/tr[4]/td[4]/b/text()')
        l.add_xpath('powers_exercised', '//center[2]/table/tr[5]/td[4]/b/text()')
        l.add_xpath('foreign_offices', '//center[2]/table/tr[9]/td[4]/b/text()')
        l.add_xpath('number_of_employees', '//center[2]/table/tr[10]/td[4]/b/text()')
        l.add_xpath('rc_balance_sheet_report', '//center[3]/table/tr[2]/td/a/@href')
        l.add_xpath('ri_income_statement_report', '//center[3]/table/tr[3]/td/a/@href')
        l.add_xpath('total_assets', '//center[4]/table/tr[3]/td[2]/b/text()')
        l.add_xpath('total_liabilities', '//center[4]/table/tr[4]/td[2]/b/text()')
        l.add_xpath('total_bank_equity_capital', '//center[4]/table/tr[5]/td[2]/b/text()')
        l.add_xpath('domestic_deposits', '//center[4]/table/tr[6]/td[2]/b/text()')
        l.add_xpath('total_deposits', '//center[4]/table/tr[7]/td[2]/b/text()')
        l.add_xpath('net_loans_leases', '//center[4]/table/tr[8]/td[2]/b/text()')
        l.add_xpath('loan_loss_allowance', '//center[4]/table/tr[9]/td[2]/b/text()')
        l.add_xpath('total_interest_income', '//center[4]/table/tr[3]/td[5]/b/text()')
        l.add_xpath('total_noninterest_income', '//center[4]/table/tr[4]/td[5]/b/text()')
        l.add_xpath('total_interest_expense', '//center[4]/table/tr[5]/td[5]/b/text()')
        l.add_xpath('total_noninterest_expense', '//center[4]/table/tr[6]/td[5]/b/text()')
        l.add_xpath('net_income', '//center[4]/table/tr[7]/td[5]/b/text()')
        l.add_xpath('net_charge_offs', '//center[4]/table/tr[8]/td[5]/b/text()')
        l.add_xpath('total_capital', '//center[4]/table/tr[12]/td[2]/b/text()')
        l.add_xpath('total_assets_leverage_ratio', '//center[4]/table/tr[13]/td[2]/b/text()')
        l.add_xpath('tier_leverage_ratio', '//center[4]/table/tr[14]/td[2]/b/text()')
        l.add_xpath('tier_capital_ratio', '//center[4]/table/tr[15]/td[2]/b/text()')
        l.add_xpath('total_capital_ratio', '//center[4]/table/tr[16]/td[2]/b/text()')
        l.add_xpath('total_loans', '//center[4]/table/tr[12]/td[5]/b/text()')
        l.add_xpath('loans_secured_real_estate', '//center[4]/table/tr[13]/td[5]/b/text()')
        l.add_xpath('commercial_industrial_loans', '//center[4]/table/tr[14]/td[5]/b/text()')
        l.add_xpath('loans_individuals_credit_cards', '//center[4]/table/tr[15]/td[5]/b/text()')
        l.add_xpath('loans_individuals_other', '//center[4]/table/tr[16]/td[5]/b/text()')
        l.add_value('url', response.url)
        return l.load_item()


class IbanknetComLoader(ItemLoader):
    def remove_trash(value):
        return ' '.join(unicode(value).split()).replace('#', '')

    def build_url(value):
        return 'http://ibanknet.com' + value

    default_output_processor = TakeFirst()
    default_input_processor = MapCompose(remove_trash)
    rc_balance_sheet_report_in = MapCompose(build_url)
    ri_income_statement_report_in = MapCompose(build_url)